# Desafio para desenvolvedor fullstack

Esse é o nosso desafio para a vaga de desenvolvedor fullstack na [Golfarma](https://golfarma.com.br). Serão testadas as habilidades e qualidade de código ao transformar requisitos limitados em uma aplicação web.

Procuramos um desenvolvedor versátil para resolver os mais diversos problemas, que impactam tanto nos processos internos como nos negocios dos nossos clientes, utilizando conhecimento em tecnologias back-end e front-end como meio para isso. Sinta-se confortável com ambas responsabilidades e seja engajado com o que há de melhor no mercado e boas práticas para trabalhar com inovação, tecnologias modernas e emergentes na Golfarma.


### Qual perfil procuramos?
- Perfil proativo, saber trabalhar em equipe , raciocínio lógico, responsabilidade e comprometimento são imprescindíveis para essa oportunidade;
- Fácil adaptação em projetos experimentais e complexos;
- Aprendizado rápido no uso de tecnologias de desenvolvimento de software;
- Experiência em Desenvolvimento de software Web.



### Instruções para o desafio

- **Fork** esse repositório e faça o desafio numa branch com o seu nome (exemplo: `nome-sobrenome`);
- Assim que concluir o seu desafio, abra um **pull request** com suas alterações.


### Desafio
- Desenvolver uma aplicação web responsável por gerenciar Vendas de Medicamentos;
- Visão de Administrador:
	- Incluir, excluir, atualizar e visualizar usuários.
- Visão de Gerente:
	- Incluir, editar e visualizar Medicamentos;
	- Visualizar vendas realizadas.
- Visão de Vendedor:
	- Visualizar Medicamentos;
	- Incluir e Excluir vendas de Medicamentos.


### Escopo do desafio
- Documentar todas suposições realizadas;
- Desenvolver os módulos de frontend e backend de forma separada;
- O desenvolvimento do back-end deve ser feito em Lavarel;
- O desenvolvimento do front-end deve utilizar JavaScript e qualquer framework ou ferramenta que suportem ou utilizem estas tecnologias;
- Preferencialmente utilizar Vue.js para o desenvolvimento do frontend;
- É aceitável utilizar algumas respostas estáticas em determinadas porções da aplicação;
- Não é necessário submeter uma aplicação que cumpra cada um dos requisitos descritos, mas o que for submetido deve funcionar;
- Informar em um arquivo INTRUÇÕES.md o passo a passo necessário para rodar o projeto (back-end e front-end) juntamente com o descritivo
das funcionalidades que foram atentidas no desafio.


### O que será avaliado
- O código será avaliado seguindo os seguintes critérios: manutenabilidade, clareza e limpeza de código; resultado funcional; entre outros fatores;
- O histórico no `git` também está avaliado;
- Não esqueça de documentar o processo necessário para rodar a aplicação;
- Se necessário explique as decisões técnicas tomadas, as escolhas por bibliotecas e ferrramentas, o uso de patterns etc.


### Diferenciais
- Adaptar a página para dispositivos móveis (torná-la responsiva);
- Utilização de boas práticas de UX na solução;
- Boa documentação de código e de serviços;
- Testes do código.

---
Em caso de dúvidas, envie um email para [gabriel.dev@golfarma.com.br](mailto:gabriel.dev@golfarma.com.br).

**Até breve**